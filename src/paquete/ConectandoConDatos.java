package paquete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConectandoConDatos {

	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3306/";
			String bbdd = "restaurante";
			String usuario = "root";
			String password = "";
			conexion = DriverManager.getConnection(servidor + bbdd, usuario, password);
		} catch (SQLException excepcion) {
			excepcion.printStackTrace();
		}
	}

	public void seleccionar() {
		String sentenciaSql = "SELECT * FROM restaurante";
		try {
			sentencia = conexion.prepareStatement(sentenciaSql);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", " + resultado.getInt(3)
						+ ", " + resultado.getInt(4) + ", " + resultado.getString(5) + ", ");
			}
		} catch (SQLException excepcion) {
			excepcion.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException excepcion) {
					excepcion.printStackTrace();
				}
			}
		}
	}

	public void insertar(String ciudad, int total_empleados, int total_mesas, String responsable) {
		try {
			String sentenciaSql = "INSERT INTO restaurante (ciudad,total_empleados,total_mesas,responsable) values (									?,?,?,?)";
			PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, ciudad);
			sentencia.setInt(2, total_empleados);
			sentencia.setInt(3, total_mesas);
			sentencia.setString(4, responsable);
			sentencia.executeUpdate();
		} catch (SQLException excepcion) {
			excepcion.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException excepcion) {
					excepcion.printStackTrace();
				}
			}
		}
	}

	public void modificar(String ciudad, String responsable) {
		try {
			String sentenciaSql = "UPDATE Restaurante set responsable=? WHERE ciudad=?";
			PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, responsable);
			sentencia.setString(2, ciudad);
			sentencia.executeUpdate();
		} catch (SQLException excepcion) {
			excepcion.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException excepcion) {
					excepcion.printStackTrace();
				}
			}
		}

	}

	public void elimiar(String ciudad) {

		try {
			String sentenciaSql = "DELETE FROM Restaurante where ciudad=?";
			PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, ciudad);
			sentencia.executeUpdate();

		} catch (SQLException excepcion) {
			excepcion.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException excepcion) {
					excepcion.printStackTrace();
				}
			}

		}
	}

	
	public void desconectar() throws SQLException {
		if(sentencia != null) {
		sentencia.close();
		}
	}
}
