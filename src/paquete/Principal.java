package paquete;

import java.sql.SQLException;
import java.util.Scanner;

public class Principal {
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		Scanner escaner = new Scanner(System.in);
		int opcion;

		do {
			System.out.println("**********************************************************");
			System.out.println("Bienvenido �Que quieres hacer?");
			System.out.println("1-Mostrar datos de los restaurantes");
			System.out.println("2-Insertar un restaurante nuevo");
			System.out.println("3-Modicar un responsable");
			System.out.println("4-Borrar un restaurante");
			opcion = escaner.nextInt();

			/*
			 * cada vez que inicializo el menu, creo un nuevo objeto,que por defecto siempre
			 * esta desconectado, por lo que no hace falta que llame al metodo desconectar.
			 * Aunque en el caso 5 le he dicho que se deconecte
			 * 
			 * 
			 */

			ConectandoConDatos datos = new ConectandoConDatos();
			datos.conectar();

			switch (opcion) {

			case 1:
				System.out.println("**********************************************************");
				System.out.println("Vamos a mostrar todos los datos de nuestros restaurantes");
				System.out.println("**********************************************************");
				System.out
						.println("El orden es: CIUDAD, TOTAL DE EMPLEADOS, TOTAL DE MESAS Y EL NOMBRE DEL RESPONSABLE");
				System.out.println("**********************************************************");
				datos.seleccionar();
				escaner.nextLine();
				break;

			case 2:
				boolean respuesta = false;
				System.out.println("**********************************************************");
				System.out.println("Has elegido crear un nuevo restaurante");
				do {
					System.out.println("**********************************************************");
					System.out.println("Vamos a insertar un restaurante nuevo.");
					System.out.println("**********************************************************");
					escaner.nextLine();
					System.out.println("Introduce la ciudad en la que se abrira el siguiente restaurante");
					String ciudad = escaner.nextLine();
					System.out.println("Introduce el numero total de empleados que tendra el nuevo local");
					int total_empleados = escaner.nextInt();
					System.out.println("Introduce el numero total de mesas que tendra el nuevo local");
					int total_mesas = escaner.nextInt();
					escaner.nextLine();
					System.out.println("Introduce el nombre del responsable");
					String responsable = escaner.nextLine();
					datos.insertar(ciudad, total_empleados, total_mesas, responsable);
					System.out.println("�Quieres crear un nuevo restaurante? Di TRUE o FALSE");
					respuesta = escaner.nextBoolean();
					if (respuesta == true) {
						respuesta = true;
					} else {
						respuesta = false;
					}
					datos.seleccionar();

				} while (respuesta);
				break;
			case 3:
				System.out.println("**********************************************************");
				System.out.println("Has elegido modificar los datos de un restaurante.");
				System.out.println("**********************************************************");
				escaner.nextLine();
				System.out.println("Introduce la ciudad des responsable que quieres cambiar");
				String ciudad = escaner.nextLine();
				System.out.println("Introduce el nuevo responsable");
				String responsable = escaner.nextLine();
				datos.modificar(ciudad, responsable);
				break;
			case 4:
				System.out.println("**********************************************************");
				System.out.println("Vamos a borrar un restaurante");
				System.out.println("**********************************************************");
				System.out.println("introduce el nombre de la ciudad que quieras borrar");
				escaner.nextLine();
				ciudad = escaner.nextLine();
				datos.elimiar(ciudad);
				datos.seleccionar();
				break;
			case 5:

				System.out.println("FIN");
				datos.desconectar();
				break;

			default:
				System.out.println("Esta opcion no esta contemplada");
				break;
			}

		} while (opcion < 5);

		escaner.close();
	}
}
