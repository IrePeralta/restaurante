CREATE DATABASE restaurante;

use restaurante;

CREATE TABLE restaurante(
id integer auto_increment primary key,
ciudad varchar (15),
total_empleados integer,
total_mesas integer,
responsable varchar (30)
);

insert into restaurante (ciudad,total_empleados, total_mesas, responsable)
values
('Zaragoza', 23, 10, 'Javier Perez'),
('Valencia', 12, 5, 'Delia Sanchez'),
('Jaén', 14, 7, 'Beatriz Collado'),
('Madrid', 15, 9, 'Angeles Vidal'),
('Valladolid', 22, 10, 'Leticia Concellon'),
('Guadalajara', 9, 5, 'Sergio Perales'),
('Granada', 17, 8, 'Ana Dominguez'),
('Huesca', 10, 6, 'Violeta Gil'),
('Cuenca', 14, 9, 'Maria Gil'),
('Logroño', 19, 2, 'Marina Perez');